const withCSS = require('@zeit/next-css')
const withMDX = require('@next/mdx')({
  extension: /\.mdx?$/
})
const GoogleFontsPlugin = require('google-fonts-plugin')

let config = {
  webpack: (config) => {
    config.plugins.push(new GoogleFontsPlugin({
      fonts: [{
        "family": "Source Sans Pro",
        "variants": [ "400", "700"]
      }, {
        "family": "Source Code Pro",
        "variants": ["400"]
      }]
    }))
/*     config.module.rules.push({
      test: /\.css$/,
      use: [
        defaultLoaders.babel,
        {
          loader: require('styled-jsx/webpack').loader
        }
      ]
    }) */

    return config
  },
  pageExtensions: ['js', 'jsx', 'md', 'mdx']
}


module.exports = withCSS(withMDX( config ))