const tailwindcss = require('tailwindcss')
const purgecss = require('@fullhuman/postcss-purgecss')({

    // Specify the paths to all of the template files in your project 
    content: [
      './components/**/*.html',
      './pages/**/*.html',
      './components/**/*.jsx',
      './pages/**/*.jsx'
    ],
  
    // Include any special characters you're using in this regular expression
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
  })

module.exports = {
    plugins: [
        require('postcss-easy-import'),
        tailwindcss('./tailwind.config.js'),
        require('autoprefixer'),
        ...process.env.NODE_ENV === 'production'
            ? [purgecss]
            : [],
        require('postcss-calc')

    ]
}