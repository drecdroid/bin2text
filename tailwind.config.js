function maxWidthsBasedOnScreen({ addUtilities, theme, e }) {
  let utilities = {}

  for(let [screenName, screenSize] of Object.entries(theme('screens'))){
    utilities[`.max-w-screen-${e(screenName)}`] = {
      'max-width': screenSize
    }
  }

  addUtilities(utilities)
}

function translate({addUtilities, theme, e}){
  let utilities = {}

  for(let [spacing, spacingValue ] of Object.entries(theme('spacing'))){
    for(let axis of ['x','y']){
      utilities[`.${e(`translate-${axis}-${spacing}`)}`] = {
        transform: `translate${axis.toUpperCase()}(${spacingValue})`
      }
    }
  }

  addUtilities(utilities)
}


function columns({ addUtilities, theme, e }){
  
}

module.exports = {
  theme: {
    fontFamily: {
      sans: ['Source Sans Pro'],
      mono: ['Source Code Pro']
    },
    extend: {}
  },
  variants: {},
  plugins: [
    maxWidthsBasedOnScreen
  ]
}
