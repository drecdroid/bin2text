import React, { createContext, useState, useEffect, useCallback } from 'react'
import NextApp, { Container } from 'next/app'
import Router from 'next/router'
import clsx from 'clsx';
if(process.browser){
  console.log('browser')
  require('pepjs')
}

import { Nav } from '../components/Nav'
import menu from './nav.json'

import './styles/index.css'
import appStyle from './styles/app.css'
import Icon from 'react-icons-kit';
import {menu as menuIcon} from 'react-icons-kit/feather/menu'

export let appContext = createContext()

function App({ children }){
  let [ navOpened, setNavOpened ] = useState(false)

  const toggleMenu = useCallback(()=>{
      setNavOpened(!navOpened)
  }, [navOpened, setNavOpened])

  useEffect(()=>{
      let listener = Router.events.on('routeChangeComplete', ()=>{
          setNavOpened(false)
          return true
      })
      return ()=>{
          Router.events.off('routeChangeComplete', listener)
      }
  }, [])

  return <appContext.Provider value={{ navOpened, setNavOpened }}>
      <section className={clsx('flex justify-center my-0 mx-auto relative max-w-full min-h-screen px-4 py-16 lg:px-8 w-full max-w-screen-xl boxed', navOpened ? 'menu-opened' : 'menu-closed' )}>
          <div className="hamburguer cursor-pointer p-6 lg:hidden lg:opacity-0 opacity-100 absolute top-0 left-0" style={{"-webkit-tap-highlight-color" : "transparent"}} onClick={toggleMenu}>
            <Icon icon={menuIcon} size={16}/>
          </div>
          <div className="cover absolute h-screen top-0 left-0 w-full z-10 hidden bg-gray-800 opacity-75 cursor-pointer" onClick={toggleMenu} />
          <aside className="aside absolute h-screen top-0 left-0 pt-16 z-20 lg:w-1/3 w-64 bg-gray-100 lg:bg-transparent"><Nav menu={menu}/></aside>
          <main className="main flex w-full flex-col">{children}</main>
      </section>
  </appContext.Provider>
}


class CustomApp extends NextApp {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = Component.getInitialProps ?
      await Component.getInitialProps(ctx) : {}

    return { pageProps }
  }

  render(){
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <App>
          <Component {...pageProps} />
        </App>
      </Container>
    );
  }
}

export default CustomApp