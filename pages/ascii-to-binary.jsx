import React from 'react'
// import PageContent from '../md/ascii-to-binary/index.mdx'
import Bin2Ascii from '../components/converters/Bin2Ascii'

function Page(){
    return <>
        <h1>ASCII to Binary Converter</h1>
        <Bin2Ascii reversed />
    </>
}


export default Page