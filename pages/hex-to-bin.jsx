import React from 'react'
import HexToBin from '../components/converters/HexToBin';
import Editor from '../components/Editor2';

function Page(){
    return <>
        <h1>Hexadecimal to Bin</h1>
        <HexToBin />
    </>
}

export default Page