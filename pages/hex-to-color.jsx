import HexToColor from "../components/converters/HexToColor";

function Page(){

    return <>
        <h1>Hexadecimal to Color</h1>
        <HexToColor />
    </>

}

export default Page