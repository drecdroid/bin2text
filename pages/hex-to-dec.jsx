import HexToDec from "../components/converters/HexToDec";

function Page(){
    return <>
        <h1>Hexadecimal to Decimal</h1>
        <HexToDec />
    </>
}

export default Page