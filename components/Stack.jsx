function Stack({ children }){
    return <div className="stack">
        {children}
        <style jsx>{`
            .stack > :global(*):not(:last-child) {
                margin-bottom: 16px;
            }
        `}</style>
    </div>
}

export default Stack