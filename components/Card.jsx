import clsx from 'clsx'

function Card({ children, padded }){

    return <div className={clsx('card', { padded : padded })}>
        {children}
        <style jsx>{`
            .card {
                overflow: hidden;
                box-shadow: 1.8px 1.8px 5.3px 0 rgba(0,0,0,0.12);
                background-color: white;
                border-radius: 1px;
            }

            .padded {
                padding: 20px;
            }
        `}</style>

        <style global jsx>{`
            h2 {
                text-align: left;
                font-size: 24pt;
                font-weight: normal;
                color: hsla(0, 0%, 35%, 1);
                margin-bottom: 8px;
            }
        `}</style>
    </div>
}

export default Card