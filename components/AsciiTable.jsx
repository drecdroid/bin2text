import React, { useState, useEffect, useRef } from 'react'
import Card from './Card';
import asciiChars from './asciiChars.json'
import Button from './Button'
import clsx from 'clsx';

const views = [
    {
        i: 0,
        view: 'decimal',
        title: 'Decimal',
        viewFn: char => char.code.toString()
    },
    {
        i: 1,
        view: 'binary',
        title: 'Binary',
        viewFn: char => char.code.toString(2).padStart(8, '0')
    },
    {
        i: 2,
        view: 'octal',
        title: 'Octal',
        viewFn: char => char.code.toString(8).padStart(3, '0')
    },
    {
        i: 3,
        view: 'hexadecimal',
        title: 'Hexadecimal',
        viewFn: char => char.code.toString(16).padStart(2, '0')
    },
    {
        i: 4,
        view: 'html-number',
        title: 'Html Number',
        viewFn: char => `&#${char.code.toString().padStart(3, '0')};`
    },
    {
        i: 5,
        view: 'html-name',
        title: 'Html Name',
        viewFn: char => char.htmlName ? "&" + char.htmlName + ";" : ''
    },
    {
        i: 6,
        view: 'description',
        title: 'Description',
        viewFn: char => char.desc
    },
/*     {
        view: 'symbol',
        title: 'Symbol',
        viewFn: char => char.symbol
    } */
]
function AsciiTable(){
    let [enabledViews, setEnabledViews] = useState(
        new Set(views.map(view => view.view))
    )

    let [outOfView, setOutOfView] = useState(false)
    let [viewWidths, setViewWidths] = useState()

    let tableRef = useRef()
    let headerRef = useRef()

    function toggleView(view){
        if(enabledViews.has(view) && enabledViews.size === 1){
            return;
        }
        let clone = new Set(enabledViews)
        if(enabledViews.has(view)){
            clone.delete(view)
            setEnabledViews(clone)
        }else{
            clone.add(view)
            setEnabledViews(clone)
        }
    }
    
    function tryPreserveHeader(e){
        console.log(e)
    }

    useEffect(()=>{
        if(headerRef.current && !viewWidths){
            let viewWidths = [].map.call(headerRef.current.querySelectorAll('th'), el => el.offsetWidth).filter((_,i)=>i)
            let bodyWidths = [].map.call(tableRef.current.querySelector('tbody > tr').querySelectorAll('td'), el => el.offsetWidth).filter((_,i)=>i)

            let widths = viewWidths.map((e,i) => Math.max(bodyWidths[i], e))
            console.log(viewWidths)
            console.log(bodyWidths)
            console.log(widths)
            setViewWidths(widths)
            setEnabledViews(new Set(['binary', 'hexadecimal']))
        }
    }, [viewWidths])
    
    useEffect(()=>{
        let listener = (e)=>{
            if(tableRef.current){
                let rect = tableRef.current.getBoundingClientRect()
                
                if(rect.top-20< 0){
                    setOutOfView(true)
                }else{
                    setOutOfView(false)
                }
            }
        }
        document.addEventListener('scroll', listener)

        return ()=>{
            document.removeEventListener('scroll', listener)
        }
    },[])

    let visibleViews = views.filter(view => enabledViews.has(view.view))

    return <Card>
            <h2>Filter</h2>
            <div className="button-list">
                {
                    views.map( view => 
                        <Button style={enabledViews.has(view.view) ? {} : {backgroundColor: '#929292' }} key={view.view} onClick={()=>{ toggleView(view.view) }}>{view.title}</Button>    
                    )
                }
            </div>
            <div className={clsx('table-wrapper', {'out-of-view': outOfView})}>
                <div className="table-scroller">
                    <table ref={tableRef} >
                        <thead ref={headerRef}>
                            <tr>
                                <th className="sticky-col">Symbol</th>
                                {
                                    visibleViews
                                    .map((view,i) => <th style={ viewWidths ? { width: `${viewWidths[i]}px`} : {}} key={view.view}>
                                        <span style={ viewWidths ? {display: 'block',  width: `${viewWidths[view.i]}px`} : { display: 'block' }}>{view.title}</span>
                                    </th>)
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                asciiChars.map( (char, i)=> {
                                    return <tr key={char.code}>
                                        <td className="sticky-col">{char.symbol}</td>
                                        {
                                            visibleViews
                                            .map((view) => <td key={view.view}><span style={ viewWidths && i === 0 ? {display: 'block',  width: `${viewWidths[view.i]}px`} : { display: 'block' }}>{view.viewFn(char)}</span></td>)
                                        }
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>    
                </div>
            </div>

        <style jsx>{`

            .button-list {
                padding: 12px 8px;
                display: flex;
                flex-wrap: wrap;
            }

            table {
                border-collapse: collapse;
                white-space: nowrap;
            }

            .table-wrapper{
                padding-top: 20px;
                position: relative;                
                padding-left: 80px;
            }

            .table-scroller {
                overflow-x: scroll;
            }

            thead {
                position: absolute;
                top: 0;
            }

            .out-of-view thead{
                position: fixed;
                top: 0;
            }

            .sticky-col {
                position: absolute;
                top: auto;
                width: 80px;
                left: 0px;
            }

            th.sticky-col {
                left: -80px;
            }
        `}</style>

        <style jsx>{`
            .button-list > :global(*) {
                margin-bottom: 8px;
            }            
        `}</style>

        <style jsx>{`
            .out-of-view table {
                padding-top: ${headerRef.current ? headerRef.current.offsetHeight : '0'}px;
            }
        `}</style>
    </Card>
}

export default AsciiTable

