import Card from './Card'

function Article({children}){

    return <>
        <Card padded >
            {children}
        </Card>

    </>
}

export default Article