import Link from 'next/link'
import { useRouter } from 'next/router'
import clsx from 'clsx';

function Nav({ menu, children }){
    let { asPath, pathname } = useRouter()

    return <nav className="nav">
        { menu ? menu.map( (group,i)=> 
            <div className="menu-group" key={group.group}>
                <NavTitle>{group.group}</NavTitle>
                {
                    group.items ? <ul>
                        { group.items.map( (item, i) => 
                            <li key={i}>
                                <Link href={item.href||"#"}><a className={clsx({ active: item.href === pathname })}>{item.text}</a></Link>
                            </li>)
                        }
                    </ul>: null
                }
            </div>
        ) : null }
        { children }
        <style jsx>{`
            .nav {
                padding-right: 32px;
                text-align: right;
            }

            .menu-group + .menu-group {
                margin-top: 32px;
            }

            .nav ul {
                padding: 0;
            }
            .nav li {
                padding: 0;
                list-style: none;
                color: hsl(0, 0%, 36%);
                text-align: right;
                margin-bottom: 8px;
                list-style: none;
                text-decoration: none;
            }

            .nav a {
                text-decoration: none;
                color: hsl(0, 0%, 35%);
            }

            .nav a.active {
                font-weight: bold;
            }
        `}</style>
    </nav>
}

function NavTitle({ children }){
    
    return <>
        <h1 className="nav-title">{children}</h1>
        <style jsx>{`
            .nav-title {
                color: #538AFD;
                text-transform: uppercase;
                font-size: 14pt;
                text-align: right;
                margin-bottom: 20px;
                font-weight: bold;
            }
        `}</style>
    </>
}

export {
    Nav,
    NavTitle
}