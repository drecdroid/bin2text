import Card from './Card'
import _ from 'underscore'

function Editor(props, ref){
    let { title, value, children } = props
    let textAreaProps = _.omit(props, 'title', 'children')
    return <Card>
        <div className="title-container">
            <span className="title">{title}</span>
        </div>

        <div className='toolbar'>{children}</div>
        <textarea className='editor' ref={ref} value={value} {...textAreaProps} />
        <style jsx>{`
            .title-container {
                display: flex;
                padding: 8px;
                align-items: center;
                height: 32px;
                background-color: hsl(0,0%,99%);
            }

            .title {
                text-transform: uppercase;
                font-size: 16px;
                color: hsl(0, 0%, 35%)
            }

            .toolbar {
                height: 48px;
                box-shadow: 1px 1px 2.6px 0 rgba(0, 0, 0, 0.07);
                display: flex;
                align-items: center;
            }

            .toolbar:empty {
                height: 1px;
            }

            .editor {
                background: none;
                width: 100%;
                resize: vertical;
            }
        `}</style>
    </Card>
}

export default React.forwardRef(Editor)