import Stack from "../Stack";
import { useEffect, useRef, useState, useMemo, useCallback, useReducer, useLayoutEffect } from "react";
import Color from 'color'
import css from 'styled-jsx/css'

function minPrecision(n, precision){
    let exp = 10 ** precision
    return (Math.floor(n * exp) / exp).toString()
}

class CSSColor {
    static rgb(color){
        color = color.rgb()
        const hasAlpha = !(color.valpha == null || color.valpha === 1)
        const rgba = 'rgb' + ( hasAlpha ? 'a' : '')
        const a = hasAlpha ? ', ' + minPrecision(color.valpha, 4) : ''
        return `${rgba}(${color.color.map(Math.round)}${a})`
    }

    static rgb100(color){

    }
    
    static hex(color){
        if(color.valpha == null || color.valpha === 1){
            return color.hex()
        }
        else{
            return color.hex() + Math.round(color.valpha*255).toString(16).toUpperCase().padStart(2, 0)
        }
    }

    static hsl(color){
        color = color.hsl()
        const h = minPrecision(color.color[0], 4)
        const s = minPrecision(color.color[1], 2)
        const l = minPrecision(color.color[2], 2)
        const hasAlpha = !(color.valpha == null || color.valpha === 1)
        const hsla = 'hsl' + ( hasAlpha ? 'a' : '')
        const a = hasAlpha ? ', ' + minPrecision(color.valpha, 4) : ''
        return `${hsla}(${h}, ${s}%, ${l}%${a})`
    }
}

function LinearSelector({ colorFromCursor, cursorFromColor, style, repaint }){

    return function({ color, onChange }){
        const background = useRef(null)
        const foreground = useRef(null)
        const cursorEl = useRef(null)
        const moving = useRef(false)
        const valueRef = useRef(cursorFromColor(color))
        const colorRef = useRef(color)
    
        const _repaint = ()=>{
            cursorEl.current.style.left = `calc(${valueRef.current*100}% - 12px)`
            repaint({
                background: background.current,
                foreground: foreground.current,
                cursor: cursorEl.current,
                value: valueRef.current,
                color: colorRef.current,
                moving: moving.current
            })
        }

        useEffect(()=>{
            valueRef.current = cursorFromColor(color)
            colorRef.current = color
            _repaint()
        }, [color])

        const loop = () => {
            if(moving.current){
                _repaint()
                onChange(valueRef.current)
            }
            frameRef.current = requestAnimationFrame(loop)
        }

        const frameRef = useRef()

        useEffect(()=>{
            let mouseup = ()=>{
                moving.current = false
                cancelAnimationFrame(frameRef.current)
                frameRef.current = null
            }

            let mousemove = (e)=>{
                if(moving.current && background.current){
                    let {left} = background.current.getBoundingClientRect()
                    let clampedX = Math.min(Math.max(e.clientX - left, 0), background.current.clientWidth) / background.current.clientWidth
                    valueRef.current = clampedX
                    if(!frameRef.current){
                        frameRef.current = requestAnimationFrame(loop)
                    }
                    e.preventDefault()
                }
            }
            
            window.addEventListener('pointerup', mouseup)
            window.addEventListener('pointermove', mousemove)

            return ()=>{
                window.removeEventListener('pointerup', mouseup)
                window.removeEventListener('pointermove', mousemove)
            }
        }, [])

        const startMoving = useCallback((e)=>{
            moving.current = true
            window.dispatchEvent(new PointerEvent('pointermove', e.nativeEvent))
        }, [])

        return <>
        <div touch-action="none" className="background relative w-full h-4 cursor-pointer" ref={background} onPointerDown={startMoving} >
            <div className="cursor absolute w-6 h-6 rounded-full border-2 border-solid border-white z-20 cursor-move" ref={cursorEl}></div>
            <div className="foreground absolute top-0 left-0 w-full h-full z-10" ref={foreground}></div>
        </div>
            <style jsx>{style}</style>
            <style jsx>{`
                .cursor {
                    top: -4px;
                }    
            `}</style>
        </>
    }
}

const alphaSelectorStyle = css`
    .background {
        background-size: 8px 8px;
        background-image:
            linear-gradient(to right, rgba(0, 0, 0, 0.3) 50%, transparent 50%, transparent),
            linear-gradient(to right, transparent 0%, transparent 50%, rgba(0, 0, 0, 0.0) 50%),
            linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 50%, transparent 50%, transparent),

            /* draw squares to selectively shift opacity */
            linear-gradient(to bottom, white 50%, transparent 50%, transparent),
            linear-gradient(to right, transparent 0%, transparent 50%, rgba(0, 0, 0, 0.5) 50%),
            
            /* b-i: l-g toggle trick */
            none;
    }
`

const AlphaSelector = LinearSelector({
    cursorFromColor(color){
        return color.alpha()
    },
    repaint({ cursor, value, color, foreground }){
        cursor.style.backgroundColor = `${color.hsl()}`
        foreground.style.background = `linear-gradient(to right, transparent, ${color.hsl().alpha(1)})`
    },
    style: alphaSelectorStyle
})

const hueSelectorStyle = css`
    .background {
        background: linear-gradient(to right, #ff0000 0%, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);
    }
`

const HueSelector = LinearSelector({
    cursorFromColor(color){
        return color.hue() / 356
    },

    repaint({ cursor, value }){
        cursor.style.backgroundColor = `hsl(${value*359}, 100%, 50%)`
    },

    style: hueSelectorStyle
})


function SVSelector({ color, onChange }){
    
    const el = useRef(null)
    const cursorEl = useRef(null)
    const moving = useRef(null)
    const hue = useMemo(()=>color.hue(), [color])
    const cursorRef = useRef({ x: color.saturationv()/100, y: 1 - color.value()/100})
    const colorRef = useRef(color)

    useEffect(()=>{
        colorRef.current = color
        refreshStyle()
    }, [color])

    const refreshStyle = () => {
        let cuEl = cursorEl.current
            cuEl.style.left = `calc(${cursorRef.current.x*100}% - 12px)`
            cuEl.style.top = `calc(${cursorRef.current.y*100}% - 12px)`,
            cuEl.style.backgroundColor = `${Color.hsv(colorRef.current.hue(), cursorRef.current.x * 100, 100-cursorRef.current.y*100).hsl().string()}`
    }

    const loop = () => {
        if(moving.current){
            refreshStyle()
            onChange(cursorRef.current.x * 100, 100 - cursorRef.current.y * 100)
        }
        frameRef.current = requestAnimationFrame(loop)
    }

    const frameRef = useRef()

    useEffect(()=>{
        function pointerup(){
            moving.current = false
            cancelAnimationFrame(frameRef.current)
            frameRef.current = null
        }
        
        function pointermove(e){
            if(moving.current && el.current){
                let { left, top } = el.current.getBoundingClientRect()
                let w = el.current.clientWidth
                let h = el.current.clientHeight
                let _left = e.clientX - left 
                let _top = e.clientY - top
                let clampedX = Math.min(Math.max(_left, 0), w) / w
                let clampedY = Math.min(Math.max(_top, 0), h) / h
                
                cursorRef.current = { x: clampedX, y:clampedY }

                if(!frameRef.current){
                    frameRef.current = requestAnimationFrame(loop)
                }
                e.preventDefault()
            }
        }

        window.addEventListener('pointerup', pointerup)
        window.addEventListener('pointermove', pointermove)

        return ()=>{
            window.removeEventListener('pointerup', pointerup)
            window.removeEventListener('pointermove', pointermove)
        }
    }, [])

    
    const startMoving = useCallback((e)=>{
        moving.current = true
        window.dispatchEvent(new PointerEvent('pointermove',  e.nativeEvent))
    }, [])

    return <>
        <div touch-action="none" className="sv relative w-full h-48 cursor-pointer" ref={el} onPointerDown={startMoving}>
            <div className="cursor absolute w-6 h-6 rounded-full border-solid border-2 border-white cursor-move" ref={cursorEl}></div>
        </div>
        <style jsx>{`
            .sv {
                background: linear-gradient(to bottom, transparent, #000),
                    linear-gradient(to right, #fff, transparent),
                    hsla(${hue}, 100%, 50%);
            }
        `}</style>
    </>
}

const validHex = /^#?([\da-f]{2}){3,4}$/i


function HexInput({ color, onChange}){
    
    const [ internalValue, setInternalValue ] = useState(CSSColor.hex(color))
    
    useEffect(()=>{
        setInternalValue(CSSColor.hex(color))
    }, [ color ])

    const hexChanges = useCallback((e)=> {
        let inputVal = e.target.value.toUpperCase()
        // Valid Final hex:
        // - #fFf
        // - #FaFaFA
        // - #FaFaFaFa
        // - fFf    
        // - FaFaFa
        // - FaFaFaFa

        if(!/^#?[\da-f]{0,8}$/i.test(inputVal)){
            return
        }
        inputVal = inputVal.startsWith('#') ? inputVal : '#' + inputVal
        setInternalValue(inputVal)
        if(validHex.test(inputVal)){
            onChange(inputVal)    
        }
    }, [])

    return <input className="w-full p-1 text-center text-xl font-bold font-mono text-gray-800 bg-gray-100"
                type="text" value={internalValue} onChange={hexChanges}></input>
}


function colorReducer(/**@type {Color} */color, action){
    console.log(color)
    if(action.type === 'set_hue'){
        return color.hue(action.hue)
    }
    else if(action.type === 'set_sv'){
        return color.saturationv(action.saturation).value(action.value)
    }
    else if(action.type === 'set_alpha'){
        return color.alpha(action.alpha)
    }
    else if(action.type === 'set_hex'){
        let alpha = action.hex.length === 9 ? parseInt(action.hex.slice(-2), 16)/255 : 1
        return color.hex(action.hex).alpha(alpha)
    }

    return color
}


function HexToColor(){
    
    let [ color, dispatch ] = useReducer(colorReducer,  Color('#5a67d8ff'))
    
    const svChanged = useCallback((saturation, value) => {
        dispatch({ type: 'set_sv', saturation, value})
    }, [])

    const hueChanged = useCallback(hue=>{
        dispatch({ type: 'set_hue', hue: hue*356 })
    }, [])

    const alphaChanged = useCallback(alpha => {
        dispatch({ type: 'set_alpha', alpha})
    }, [])

    const hexChanged = useCallback(hex => {
        dispatch({ type: 'set_hex', hex })
    }, [])


    return <Stack>
            <div className="flex p-4 bg-white shadow-md rounded-sm">
                <HexInput color={color} onChange={hexChanged}/>
            </div>
            <div className="flex p-4 bg-white shadow-md rounded-sm flex-wrap">
                <div className="preview relative lg:w-64 lg:h-auto lg:mr-4 lg:mb-0 w-full h-16 shadow mb-4">
                    <div className="color absolute left-0 top-0 w-full h-full" style={{backgroundColor: color.hsl().string()}}></div>
                </div>
                <div className="flex-grow lg:w-auto w-full">
                    <Stack>
                        <HueSelector color={color} onChange={hueChanged} />
                        <AlphaSelector color={color} onChange={alphaChanged} />
                        <SVSelector color={color} onChange={svChanged} />
                </Stack>
                </div>
            </div>
            <div className="p-4 bg-white shadow-md rounded-sm">
                <Stack>
                    <h2>CSS Color Strings</h2>
                    <pre className="p-4 whitespace-pre-wrap" style={{backgroundColor: color.hsl().saturationl(80).lightness(95).alpha(1)}}>{
                        `HEXa:\t${CSSColor.hex(color)}\n`+
                        `HSLa:\t${CSSColor.hsl(color)}\n`+
                        `RGBa:\t${CSSColor.rgb(color)}`
                    }
                    </pre>
                </Stack>
            </div>
            <style jsx>{`
                .preview {
                    background-size: 32px 32px;
                    background-image:
                        linear-gradient(to right, rgba(0, 0, 0, 0.3) 50%, transparent 50%, transparent),
                        linear-gradient(to right, transparent 0%, transparent 50%, rgba(0, 0, 0, 0.0) 50%),
                        linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 50%, transparent 50%, transparent),

                        /* draw squares to selectively shift opacity */
                        linear-gradient(to bottom, white 50%, transparent 50%, transparent),
                        linear-gradient(to right, transparent 0%, transparent 50%, rgba(0, 0, 0, 0.5) 50%),
                        
                        /* b-i: l-g toggle trick */
                        none;

                }
            `}</style>
        </Stack>
}

export default HexToColor