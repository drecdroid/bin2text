import { useReducer, useMemo, useCallback } from 'react'
import Editor from '../Editor2'

import Stack from '../Stack';
import { changeBase, baseReducer } from './changeBase';


function HexToBin(){
    const [ state, dispatch ]  = useReducer(baseReducer, 0)
    const changeHex = useMemo(()=>changeBase(16, dispatch), [dispatch])
    const changeBin = useMemo(()=>changeBase(2, dispatch), [dispatch])
    
    return <Stack>
        <Editor title="hexadecimal" value={state.toString(16).toUpperCase()} onChange={changeHex} />
        <Editor title="binary" value={state.toString(2)} onChange={changeBin} />
    </Stack>
}

 export default HexToBin