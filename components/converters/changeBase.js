import bigInt from "big-integer"

const symbols = '0123456789ABCDEFGHIJKLMNOPQRSTUV'

function changeBase(base, dispatch){
    let invalidCharsRegex = new RegExp(`[^${symbols.slice(0,base)}]`, 'i')
    return function handler(e){
        if(invalidCharsRegex.test(e.target.value)) return;

        dispatch({
            type: 'set',
            number: e.target.value,
            base: base
        })
    }
}

function baseReducer(state, action){
    if(action.type === 'set'){
        return bigInt(action.number, action.base) || bigInt.zero
    }
}

export { changeBase, baseReducer }