import React from 'react'
import { useState } from "react"
import { BinEditor, OctalEditor } from '../BaseEditor'

function changeStringBase(str, fromBase, toBase, groupSize, fill='0'){
    return str.split(' ')
        .map(c => parseInt(c, fromBase))
        .filter(n => !isNaN(n))
        .map(n => n.toString(toBase).padStart(groupSize, fill))
        .join('')
}

function Bin2Oct({ reversed }){
    let [oct, setOct] = useState('')
    let [bin, setBin] = useState('')

    function onBinChange(bin){
        setOct(changeStringBase(bin, 2, 16, 3))
    }

    function onOctChange(hex){
        setBin(changeStringBase(hex, 8, 2, 8))
    }

    const binEditor = <BinEditor
        title="Binary"
        placeholder="Write or paste your binary here. Only 1s and 0s are allowed."
        key="binEditor"
        value={bin}
        onChange={onBinChange}
    />

    const octEditor = <OctalEditor
        title="Octal"
        placeholder="Write or paste your octal here. Only 0 to 7 allowed."
        key="octEditor"
        value={oct}
        onChange={onOctChange}
    />

    const toRender = [
        binEditor,
        <br key="br" />,
        octEditor,
    ]
    
    if(reversed){ toRender.reverse() }
    return <>
        {toRender}       
    </>
}

export default Bin2Oct
