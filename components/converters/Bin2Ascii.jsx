import React from 'react';
import { useState } from "react";
import { ASCIIByteEditor } from '../BaseEditor';
import Editor from '../Editor';
import Button from '../Button';

function Bin2Ascii({ reversed }){
    let [ascii, setAscii] = useState('')
    let [binText, setBinText] = useState('')

    function onBinEditorChange(bin){
        let newAscii = bin.split(' ').map(c => parseInt(c, 2)).filter(n => !isNaN(n)).map(n => String.fromCharCode(n)).join('')
        setAscii(newAscii)
    }

    function onTextChange(event){
        let textValue = event.target.value
        let newBinText = textValue.replace(/(.|\n|\r\n|\r)/g, (m)=>{
            let code = m.charCodeAt()

            if(code > 127 ){
                return ''
            }
            else{
                return ' ' + code.toString(2).padStart(8,0)
            }
        }).slice(1)


        setBinText(newBinText)
        setAscii(textValue)
    }
    
    const binEditor = <ASCIIByteEditor
        title="BINARY ASCII BYTES"
        placeholder="Write or paste your binary here. Only 1s and 0s are allowed."
        key="binEditor"
        value={binText}
        onChange={onBinEditorChange}
    />
    
    const textEditor = <Editor
        key="textEditor"
        title="ascii text"
        value={ascii}
        onChange={onTextChange}
        placeholder="Write or paste your ASCII text here. Only ASCII characters are allowed.">
            <Button>Copy</Button>
        </Editor>
    
    const toRender = [
        binEditor,
        <br key="br" />,
        textEditor
    ]
    
    if(reversed){ toRender.reverse() }
    return <>
        {toRender}       
    </>
}

export default Bin2Ascii