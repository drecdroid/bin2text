
function debounce(fn, time){
    let busy = false

    return function debounced(...args){
        if(!busy){
            fn(...args)
            busy = true
            setTimeout(()=>{
                busy = false
            }, time)
        }
    }
}

class HistoryStack {

    currentIndex = 0
    
    constructor(initial, maxStackSize=50){
        this.maxStackSize = maxStackSize
        this.stack = [initial]
    }

    get current(){
        return this.stack[this.currentIndex]
    }

    undo(){
        if(this.currentIndex){
            this.currentIndex--
        }
    }

    redo(){
        if(this.currentIndex < this.stack.length - 1) {
            this.currentIndex++
        }
    }

    addState = debounce((state) => {
        if(this.stack.length === this.maxStackSize){
            this.stack = this.stack.slice(1, this.currentIndex+1).concat(state)
        }
        else{
            this.stack = this.stack.slice(0, this.currentIndex+1).concat(state)
        }
        this.currentIndex = this.stack.length - 1
    }, 16)

}

let digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'


function BaseEditor({ value='', onChange, placeholder, groupSize, base, children }){
    let editor = useRef()
    let caretPosRef = useRef()
    let textHistory = useMemo(()=>new HistoryStack(value), [])
    let caretHistory = useMemo(()=>new HistoryStack(0), [])

    useEffect(()=>{
        if(editor.current){
            document.execCommand('enableObjectResizing', 1)
        }
    }, [])

    let invalidChars = new RegExp(`[^${digits.slice(0,base)}]`, 'gi')
    let testChars = new RegExp(`[^${digits.slice(0,base)}\\s]`, 'i')
    let groupRegex = new RegExp(`.{1,${groupSize}}`, 'g')

    useEffect(()=>{
        textHistory.addState(value)
        caretHistory.addState(0)
        let text = textHistory.current
        let buff = text.replace(invalidChars, '').toUpperCase().replace(groupRegex, m => {
            return `<i>${m}</i>`
        })
        editor.current.innerHTML = buff
    }, [value])


    let processLetter = useCallback((event)=>{
        
        if(event.nativeEvent.inputType &&  event.nativeEvent.inputType === 'historyUndo'){
            caretHistory.undo()
            textHistory.undo()
        }

        else if(event.nativeEvent.inputType &&  event.nativeEvent.inputType === 'historyRedo'){
            caretHistory.redo()
            textHistory.redo()
        }

        else if(editor.current){
            let text = event.target.innerText 

            if(testChars.test(text)){
                let caretPos = position(event.target)
                editor.current.innerHTML = textHistory.current
                
                position(event.target, caretPos.pos-1)
                
                event.preventDefault()
                
                return
            }
            caretPosRef.current = position(event.target).pos
            let buff = text.replace(invalidChars, '').toUpperCase().replace(groupRegex, m => {
                return `<i>${m}</i>`
            })

            textHistory.addState(buff)
            caretHistory.addState(caretPosRef.current)
        }

        editor.current.innerHTML = textHistory.current
        position(editor.current, caretHistory.current)
        if(onChange && onChange instanceof Function){
            onChange(editor.current.innerText.replace(/\s/g,''))
        }
    }, [])

    return <>
        <div ref={editor} className="editor" contentEditable onInput={processLetter}></div>
        <style jsx>{`
            .editor {
                padding: 16px;
                padding-left: 8px;
                padding-right: 8px;
                font-size: 16px;
                background-color: white;
                max-width: 100%;
                display: flex;
                flex-wrap: wrap;
                border-radius: 4px;
                overflow-y: scroll;
                resize: vertical;
                align-content: flex-start;
            }    

            .editor:empty::before {
                color: #636363;
            }
            .editor :global(i) {
                padding-left: 8px;
                padding-right: 8px;
                font-style: normal;
            }
            
            .editor:empty {
                padding-left: 16px;
            }
        `}</style>
        <style jsx>{`
            .editor:empty::before{
                content: "${placeholder || ''}";
                padding-right: 8px;
            }
        `}</style>
    </>
}