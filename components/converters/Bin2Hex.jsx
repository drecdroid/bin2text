import React from 'react'
import { useState } from "react"
import { BinEditor, HexEditor } from '../BaseEditor'

function changeStringBase(str, fromBase, toBase, groupSize, fill='0'){
    return str.split(' ')
        .map(c => parseInt(c, fromBase))
        .filter(n => !isNaN(n))
        .map(n => n.toString(toBase).padStart(groupSize, fill))
        .join('')
}

function Bin2Hex({ reversed }){
    let [hex, setHex] = useState('')
    let [bin, setBin] = useState('')

    function onBinChange(bin){
        setHex(changeStringBase(bin, 2, 16, 2))
    }

    function onHexChange(hex){
        setBin(changeStringBase(hex, 16, 2, 8))
    }

    const binEditor = <BinEditor
        title="Binary"
        placeholder="Write or paste your binary here. Only 1s and 0s are allowed."
        key="binEditor"
        value={bin}
        onChange={onBinChange}
    />

    const hexEditor = <HexEditor
        title="Hexadecimal"
        placeholder="Write or paste your hexadecimal here. Only 0 to 9 and A to F characters allowed."
        key="hexEditor"
        value={hex}
        onChange={onHexChange}
    />

    const toRender = [
        binEditor,
        <br key="br" />,
        hexEditor,
    ]
    
    if(reversed){ toRender.reverse() }
    return <>
        {toRender}       
    </>
}

export default Bin2Hex
