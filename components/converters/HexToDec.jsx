import { useReducer, useMemo, useCallback } from 'react'
import Editor from '../Editor2'

import Stack from '../Stack';
import { changeBase, baseReducer } from './changeBase';


function HexToDecimal(){
    const [ state, dispatch ]  = useReducer(baseReducer, 0)
    const changeHex = useMemo(()=>changeBase(16, dispatch), [dispatch])
    const changeBin = useMemo(()=>changeBase(10, dispatch), [dispatch])
    
    return <Stack>
        <Editor title="hexadecimal" value={state.toString(16).toUpperCase()} onChange={changeHex} />
        <Editor title="decimal" value={state.toString()} onChange={changeBin} />
    </Stack>
}

 export default HexToDecimal