import Card from './Card'
import Button from './Button'
import { Icon } from 'react-icons-kit'
import {copy} from 'react-icons-kit/feather/copy'
import {clipboard} from 'react-icons-kit/feather/clipboard'
import {file} from 'react-icons-kit/feather/file'


function Editor({ title, placeholder, value, onChange }){

    return <Card>
        <header className="relative shadow">
            <div className="p-2 h-8 bg-indigo-600 flex items-center">
                <span className="uppercase text-base text-white">{title}</span>
            </div>
            <div className="toolbar h-10 flex items-center">
                <Button><Icon icon={file} size={16} className="icon"/></Button>
                <Button><Icon icon={copy} size={16} className="icon"/></Button>
                <Button><Icon icon={clipboard} size={16} className="icon"/></Button>
            </div>
        </header>
        <textarea className="block editor overflow-y-auto text-gray-800 w-full p-4 text-2xl resize-y font-mono" placeholder={placeholder} onChange={onChange} value={value} />
    </Card>
}

export default Editor