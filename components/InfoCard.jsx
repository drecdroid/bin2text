import Article from './Article'

function InfoCard({title, children}) {
    return <Article>
        <h2>{title}</h2>
        <p>{children}</p>
    </Article>
}

export default InfoCard