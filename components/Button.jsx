import React from 'react'
import _omit from 'lodash/omit'

function Button(props){
    let {children} = props
    let buttonProps = _omit(props, 'children')
    return <>
        <button {...buttonProps}>
            <span>
                {children}
            </span>
        </button>
        <style jsx>{`
            button:hover, button:focus {
                background-color: #fafafa;
            }

            button:focus {
                outline: 1px solid #ceddff;
            }

            button {
                background-color: white;
                color: #525252;
                border: none;
                outline: none;
                height: 32px;
                line-height: 16px;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 2px;
                margin-left: 4px;
                cursor: pointer;
                padding: 8px;
                padding-top: 6px;
            }
            
            button span {
                font-size: 16px;
            }
            
        `}</style>
    </>
}

export default Button