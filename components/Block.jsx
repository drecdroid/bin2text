import dynamic from 'next/dynamic'
import { useState, useEffect } from 'react';
import mdx from "@mdx-js/mdx"

function Block({ path }){
    let [ Component, setComponent ] = useState(null)
    useEffect(()=>{
        let module = require('../md/'+ path)
        console.log(module.default())
        //setComponent(module.default)
    }, [])

    return Component ? <Component /> : <></>
}

export default Block