import React, { useState, useRef, useEffect, useMemo } from 'react'
import Editor from './Editor';
import Button from './Button';

let symbols = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

function formatBase({base=2, groupSize=8, maxSymbols=8, fill='0'}){
    let nonValidChars = new RegExp(`[^${symbols.slice(0,base)}]`, 'g')
    let textGrouper = new RegExp(`.{1,${groupSize}}`, 'g')
    return function(text){
        return (text.toUpperCase().replace(nonValidChars, '')
            .match(textGrouper) || [])
            .map( c => {
                let left = fill.repeat(groupSize-maxSymbols)
                let right = c.slice(groupSize-maxSymbols)
                return left + right
            })
            .join(' ')
    }
}


function BaseEditor({ base, groupSize, maxSymbols, fill, name}){
    function _BaseEditor({ title, onChange, placeholder, value }){
        let [reprText, setReprText] = useState(value)
        let selection = useRef({ start: 0, end: 0 })
        let [caretPos, setCaretPos] = useState(0)
        let textArea = useRef(null)

        let format = useMemo(()=>{
            return formatBase({ base, groupSize, maxSymbols, fill })
        }, [ base, groupSize, maxSymbols, fill ])

        useEffect(()=>{
            if(textArea.current){
                textArea.current.setSelectionRange(caretPos, caretPos)
            }
        }, [ reprText ])

        useEffect(()=>{
            setReprText(format(value))
        }, [value])

        function onChangeHandler(event){
            let inputText = event.target.value
            let caretPos = event.target.selectionEnd
            let diff = inputText.slice(selection.current.start, caretPos).replace(/\s/g, '')
            let newCaretPos = selection.current.start + diff.length + Math.floor(diff.length/base) + 1 
            let newReprText = format(inputText)

            if(onChange){
                onChange(newReprText)
            } 
            setReprText(newReprText)
            setCaretPos(newCaretPos)
        }

        function onSelectHandler(event){
            let target = event.target
            selection.current = { start: target.selectionStart, end: target.selectionEnd}
        }

        async function copy(){
            try{
                navigator.clipboard.writeText(reprText)
            }
            catch(e){
                console.log(e)
            }
        }

        async function copyWithoutSpaces(){
            try{
                navigator.clipboard.writeText(reprText.replace(/\s/g,''))
            }
            catch(e){
                console.log(e)
            }
        }
        
        return <>
            <Editor title={title} placeholder={placeholder} ref={textArea} value={reprText} onChange={onChangeHandler} onSelect={onSelectHandler}>
                <Button onClick={copy}>Copy</Button>
                <Button onClick={copyWithoutSpaces}>Copy without Spaces</Button>
            </Editor>
        </>
    }

    name && (_BaseEditor.displayName = `BaseEditor(${name})`)
    return _BaseEditor
}

const ASCIIByteEditor = BaseEditor({
    base: 2,
    fill: '0',
    groupSize: 8,
    maxSymbols: 7,
    name: 'ASCIIByteEditor'
})

const BinEditor = BaseEditor({
    base: 2,
    fill: '0',
    groupSize: 8,
    maxSymbols: 8,
    name: 'BinaryEditor'
})

const OctalEditor = BaseEditor({
    base: 8,
    groupSize: 3,
    maxSymbols: 3,
    name: 'OctalEditor'
})

const DecimalEditor = BaseEditor({
    base: 10,
    groupSize: 3,
    maxSymbols: 3,
    name: 'DecimalEditor'
})

const HexEditor = BaseEditor({
    base: 16,
    groupSize: 2,
    maxSymbols: 2,
    name: 'HexEditor'
})

export {
    BaseEditor,
    ASCIIByteEditor,
    BinEditor,
    OctalEditor,
    HexEditor
}