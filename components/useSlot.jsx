import React, {useMemo, useReducer, useEffect, useContext} from "react"
import _get from 'lodash.get'

function tryPass(els, props){
    return [].map.call(els, (el,i) => <React.Fragment key={i}>
        {el instanceof Function ? el(props) : React.cloneElement(el, props)}
    </React.Fragment>)
}

let defaultSlotName = 'default-slot'
let defaultSlot = Symbol(defaultSlotName)

let SlotCtx = React.createContext()

function Slotted({props, children}){
    let slots = useMemo(()=>{
        let slots = Object.create(null)
        let _children = props.children ?
          (props.children.length ? props.children : [props.children]) :
          []
            
      
        for(let child of _children){
          const slotName = (child.props && child.props.slot) || defaultSlot;
          (slots[slotName] || (slots[slotName] = [])).push(child)
        }
        return slots    
    }, [props.children])

    return <SlotCtx.Provider value={{props, slots}}>{children}</SlotCtx.Provider>
  }
  
function Slot({ name=defaultSlot, children }){
    let { slots, props } = useContext(SlotCtx)
    let elements = slots[name] || children || []
        
    return <React.Fragment>
        {elements}
    </React.Fragment>
}

  
function useSlots(props){
/*     let slots = useMemo(()=>{
        let slots = new Map()
        let children = props.children ?
            (props.children.length ? props.children : [props.children]) :
            []
            

        for(let child of children){
            const slotName = (child.props && child.props.slot) || defaultSlot;
            if(!slots.get(slotName)) slots.set(slotName, []);
            slots.get(slotName).push(child)
        }

        return slots
    }, [props.children])
    console.log(props)
    const Slot = useMemo(()=>{
        function Slot({ name, children }) {
            let els = slots.get(name) || []
    
            return <React.Fragment key={name}>{
                els.map(el => 
                    _get(el, 'props.scoped') && _get(el, 'props.children') instanceof Function ?
                        React.cloneElement(el, {}, [el.props.children(props)]) :
                        tryPass(els && els.length ? els : children, props)
                )
            }</React.Fragment>
        }

        function DefaultSlot({ children }){
            let elements = slots.get(defaultSlot) || []
            console.log(props)
            return <React.Fragment>
                {tryPass(elements && elements.length ? elements : children, props)}
            </React.Fragment>
            return <>{props.children}</>
        }

        Slot.Default = DefaultSlot
        return Slot
    }, [props])

    return Slot */

    function Slot(){
        return <React.Fragment key="same">{props.children}</React.Fragment>
    }

    return Slot 
}

export { Slot, Slotted }
export default useSlots