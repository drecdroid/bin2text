## Setup
Run yarn to install dependencies, not npm.
```
yarn
```

## Run
```
yarn dev
```

## TODO
- [ ] Upgrade dependencies
    - [ ] @fortawesome/fontawesome-svg-core 1.2.19  1.3.0    6.1.1  dependencies    https://fontawesome.com                                                   
    - [ ] @fortawesome/free-solid-svg-icons 5.9.0   5.15.4   6.1.1  dependencies    https://fontawesome.com                                                   
    - [ ] @fortawesome/react-fontawesome    0.1.4   0.1.18   0.1.18 dependencies    https://github.com/FortAwesome/react-fontawesome                          
    - [ ] @fullhuman/postcss-purgecss       1.2.0   1.3.0    4.1.3  devDependencies https://purgecss.com                                                      
    - [ ] @mdx-js/loader                    1.0.21  1.6.22   2.1.1  dependencies    https://mdxjs.com                                                         
    - [ ] @next/mdx                         8.1.0   8.1.0    12.1.6 dependencies    https://github.com/vercel/next.js#readme                                  
    - [ ] @types/react                      16.8.22 16.14.26 18.0.9 devDependencies https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react
    - [ ] autoprefixer                      9.6.1   9.8.8    10.4.7 devDependencies https://github.com/postcss/autoprefixer#readme                            
    - [ ] babel-plugin-macros               2.6.1   2.8.0    3.1.0  devDependencies https://github.com/kentcdodds/babel-plugin-macros#readme                  
    - [ ] big-integer                       1.6.44  1.6.51   1.6.51 dependencies    https://github.com/peterolson/BigInteger.js#readme                        
    - [ ] caret-pos                         1.2.1   1.2.2    2.0.0  dependencies    https://github.com/deshiknaves/caret-pos#readme                           
    - [ ] clsx                              1.0.4   1.1.1    1.1.1  dependencies    https://github.com/lukeed/clsx#readme                                     
    - [ ] color                             3.1.2   3.2.1    4.2.3  dependencies    https://github.com/Qix-/color#readme                                      
    - [ ] cssnano                           4.1.10  4.1.11   5.1.8  devDependencies https://github.com/cssnano/cssnano                                        
    - [ ] google-fonts-plugin               5.0.0   5.0.2    5.0.2  devDependencies https://github.com/SirPole/google-fonts-plugin#readme                     
    - [ ] mdx.macro                         0.2.8   0.2.9    0.2.9  dependencies    https://github.com/frontarm/mdx-util#readme                               
    - [ ] next                              8.1.0   8.1.0    12.1.6 dependencies    https://nextjs.org                                                        
    - [ ] pepjs                             0.5.2   0.5.3    0.5.3  dependencies    https://github.com/jquery/PEP#readme                                      
    - [ ] postcss-calc                      7.0.1   7.0.5    8.2.4  devDependencies                                                                           
    - [ ] postcss-easy-import               3.0.0   3.0.0    4.0.0  devDependencies https://github.com/TrySound/postcss-easy-import                           
    - [ ] react                             16.8.6  16.14.0  18.1.0 dependencies    https://reactjs.org/                                                      
    - [ ] react-dom                         16.8.6  16.14.0  18.1.0 dependencies    https://reactjs.org/                                                      
    - [ ] react-icons-kit                   1.3.1   1.3.1    2.0.0  dependencies    https://github.com/wmira/react-icons-kit#readme                           
    - [ ] styled-jsx                        3.2.1   3.4.7    5.0.2  dependencies    https://github.com/vercel/styled-jsx#readme                               
    - [ ] tailwindcss                       1.0.5   1.9.6    3.0.24 devDependencies https://tailwindcss.com                                                   
    - [ ] underscore                        1.9.1   1.13.3   1.13.3 dependencies    https://underscorejs.org
